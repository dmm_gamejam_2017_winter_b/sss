﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ResultBackground : MonoBehaviour, IPointerClickHandler {

	public ResultScreen resultScreen;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
	}

	public void OnPointerClick(PointerEventData eventData) {
		resultScreen.m_isFade = true;
	}
}
