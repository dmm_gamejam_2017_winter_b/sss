﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartButton : MonoBehaviour, IPointerClickHandler {

	public TitleScene titleScene;
	// Use this for initialization
	void Start()
	{
	}

	public void OnPointerClick(PointerEventData eventData) {
		titleScene.m_isFade = true;
	}
}
