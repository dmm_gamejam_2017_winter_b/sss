﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour
{
	Text m_ResultText;


	private float count = 0.0f;
	public bool m_isFade = false;
	public float m_fadeTiming = 120.0f;

	public AudioClip currentBGM;
	public AudioClip nextBGM;
	public AudioSource audioSource;
	// Use this for initialization
	void Start()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = currentBGM;
		audioSource.Play();
		//SceneManager.GetInstance().SetCurrentBGM(currentBGM);

		m_ResultText = GameObject.Find ("score").GetComponent<Text> ();
		m_ResultText.text = SceneManager.GetInstance().score.ToString();

		int score = SceneManager.GetInstance ().score;
		if (score > 8000) {
			GameObject.Find ("res4").SetActive (true);
			GameObject.Find ("res3").SetActive (false);
			GameObject.Find ("res2").SetActive (false);
			GameObject.Find ("res1").SetActive (false);
		}
		else if (score > 6000) {
			GameObject.Find ("res4").SetActive (false);
			GameObject.Find ("res3").SetActive (true);
			GameObject.Find ("res2").SetActive (false);
			GameObject.Find ("res1").SetActive (false);
		}
		else if (score > 4000) {
			GameObject.Find ("res4").SetActive (false);
			GameObject.Find ("res3").SetActive (false);
			GameObject.Find ("res2").SetActive (true);
			GameObject.Find ("res1").SetActive (false);
		}
		else {
			GameObject.Find ("res4").SetActive (false);
			GameObject.Find ("res3").SetActive (false);
			GameObject.Find ("res2").SetActive (false);
			GameObject.Find ("res1").SetActive (true);
		}

			
	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown("space"))
		{
			m_isFade = true;
		}

		if (m_isFade)
		{
			count += 240.0f * Time.deltaTime;

			if (count >= m_fadeTiming)
			{
				SceneManager.GetInstance().ChangeScene("title", nextBGM);
			}
		}
		else
		{
			count = 0;
		}
	}
}

