﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Girl : MonoBehaviour {

	public enum ClothingState { Full, TopOnly, BottomOnly, None }
	ClothingState m_ClothingState = ClothingState.Full;

	Image m_Image;

	public Sprite m_FullSprite;
	public Sprite m_TopOnlySprite;
	public Sprite m_BottomOnlySprite;
	public Sprite m_NoneSprite;

	Vector2 m_MoveVector;

	public float m_HitTimer = 1;

	void Initialize() {
		m_Image = GetComponent<Image> ();
	}

	// Use this for initialization
	void Start () {
		SetClothingState (ClothingState.Full);
	}
	
	// Update is called once per frame
	void Update () {
		float normalMoveSpeed = 200;
		float panicMoveSpeed = 400;

		float currentSpeed = m_ClothingState == ClothingState.Full ? normalMoveSpeed : panicMoveSpeed;
		Vector2 movement = m_MoveVector * currentSpeed * Time.deltaTime;

		transform.position = transform.position + new Vector3 (movement.x, movement.y, 0);

		if (Input.GetKeyDown (KeyCode.T))
			OnTopHit ();
		else if (Input.GetKeyDown (KeyCode.B))
			OnBottomHit ();
	}

	public void OnTopHit() {
		if (m_ClothingState == ClothingState.Full)
			SetClothingState(ClothingState.BottomOnly);
		else if (m_ClothingState == ClothingState.TopOnly)
			SetClothingState(ClothingState.None);
	}

	public void OnBottomHit() {
		if (m_ClothingState == ClothingState.Full)
			SetClothingState(ClothingState.TopOnly);
		else if (m_ClothingState == ClothingState.BottomOnly)
			SetClothingState(ClothingState.None);
	}

	public Vector2 GetMoveVector() {
		return m_MoveVector;
	}

	public void SetMoveVector(Vector2 moveVector) {
		m_MoveVector = moveVector;
		m_Image = GetComponent<Image> ();

		if (m_Image.sprite != null) {
			Vector3 scale = transform.localScale;
			scale.x = moveVector.x > 0 ? -scale.x : scale.x;
			transform.localScale = scale;
		}
	}

	public ClothingState GetClothingState() {
		return m_ClothingState;
	}

	void SetClothingState(ClothingState newState) {
		ClothingState prevState = m_ClothingState;
		m_ClothingState = newState;

		Sprite sprite;

		switch (m_ClothingState) {
		case ClothingState.Full:
			m_Image.sprite = m_FullSprite;
			break;
		case ClothingState.TopOnly:
			m_Image.sprite = m_TopOnlySprite;
			break;
		case ClothingState.BottomOnly:
			m_Image.sprite = m_BottomOnlySprite;
			break;
		case ClothingState.None:
			m_Image.sprite = m_NoneSprite;
			break;
		}

		if (prevState != m_ClothingState && prevState == ClothingState.Full) {
			SetMoveVector (Random.value > 0.5 ? Vector2.right : Vector2.left);
		}
	}
}
