﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameScene : MonoBehaviour
{
    // 
    private int[,] data = new int[,] {
        { 100, 5},
        { 100, 10},
        { 400, 1},
        { 50, 10},
        { 50, 10},
        { 60, 25},
        { 60, 15},
        { 60, 15},
        { 60, 15},
        { 60, 15},
    };

    public GameObject[] datas;
    public GameObject canvasObject;
    private GameObject currentEnemy;
    public float m_fadeTiming = 120.0f;
    public Text timerText; //Text用変数
    public Text scoreText; //Text用変数
    public Text hpText; //Text用変数
    public GameObject Gauge;
    public GameObject Cursor;
    public GameObject effect;
    public float timerCount = 60;
    private int score = 0;
    public float cursorSpeed = 5;
    private float gaugeStack = 0;
    private Vector2 GaugeSize;
    private Vector3 GaugePos;

    public AudioClip currentBGM;
    public AudioClip nextBGM;
    private int sum = 0;
    private int[] per = new int[1000];
    private int currentIndex = 0;
    public int currentEnemyHp = 0;
    public int currentEnemyMaxHp = 0;
    private bool is_enemy_dead = false;
    private bool m_isFade = false;
    private float count = 0.0f;

    private SceneCarrier sceneCarrier;

    // Use this for initialization
    void Start()
    {
        sceneCarrier = GetComponent<SceneCarrier>();
        //SceneManager.GetInstance().SetCurrentBGM(currentBGM)
        SceneCarrier.SetScore(0);
        timerText.text = "" + Mathf.FloorToInt(timerCount).ToString();
        scoreText.text = "";
        hpText.text = "";
        GaugeSize = Gauge.GetComponent<RectTransform>().sizeDelta;
        GaugePos = Cursor.GetComponent<RectTransform>().position;

        // 確率配列の作成
        int index = 0;
        for (int i = 0; i < data.Length * 0.5f; i++)
        {
            sum += data[i , 0];

            for(int j = 0; j < data[i, 0]; j++)
            {
                per[index] = i;
                index++;
            }
        }

        // 最初の敵作成
        currentIndex = per[Random.Range(0, 999)];
        currentEnemyHp = data[currentIndex, 1];
        currentEnemyMaxHp = currentEnemyHp;
        currentEnemy = (GameObject)Instantiate(datas[currentIndex], new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
        currentEnemy.transform.SetParent(canvasObject.transform, false);
        is_enemy_dead = false;

    }

    // Update is called once per frame
    void Update()
    {
        // データをひろってくる
        if (is_enemy_dead)
        {
            is_enemy_dead = false;

            // 指定された敵を出す
            Destroy(currentEnemy);
            currentIndex = per[Random.Range(0, 999)];
            currentEnemyHp = data[currentIndex, 1];
            currentEnemyMaxHp = currentEnemyHp;
            currentEnemy = (GameObject)Instantiate(datas[currentIndex], new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
            currentEnemy.transform.SetParent(canvasObject.transform, false);
            hpText.text = "HP " + Mathf.FloorToInt(currentEnemyHp).ToString() + "/" + Mathf.FloorToInt(currentEnemyMaxHp).ToString();
        }

        timerCount -= Time.deltaTime;
        if (timerCount <= 0)
        {
            timerCount = 0;
        } 
        timerText.text = "" + Mathf.FloorToInt(timerCount).ToString();
        Vector3 pos = Cursor.GetComponent<RectTransform>().position;

        gaugeStack += Mathf.Abs(cursorSpeed);

        pos.y -= cursorSpeed;
        if (gaugeStack > GaugeSize.y)
        {
            gaugeStack = GaugeSize.y;
            gaugeStack = 0;
            cursorSpeed *= -1;
        }

        Cursor.GetComponent<RectTransform>().position = new Vector3(pos.x, pos.y, pos.z);

        // 押したとき
        if (Input.GetKeyDown("space") && (is_enemy_dead == false))
        {
            GameObject effects = (GameObject)Instantiate(effect, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
            effects.transform.SetParent(canvasObject.transform, false);
            if (cursorSpeed < 0)
            {
                cursorSpeed *= -1;
            }
            Cursor.GetComponent<RectTransform>().position = new Vector3(pos.x, GaugePos.y, pos.z);

            // ゲージの値を取得して攻撃
            float power = gaugeStack / GaugeSize.y;
            int damage = 0;
            if (0 < power && power < 0.18)
            {
                damage = 0;
            }
            else if (0.18 < power && power < 0.284)
            {
                damage = 1;
            }
            else if (0.284 < power && power < 0.42)
            {
                damage = 2;
            }
            else if (0.42 < power && power < 0.42)
            {
                damage = 3;
            }
            else if (0.42 < power && power < 0.45)
            {
                damage = 0;
            }
            else if (0.45 < power && power < 0.5)
            {
                damage = 5;
            }
            else if (0.5 < power && power < 0.534)
            {
                damage = 0;
            }
            else if (0.534 < power && power < 0.715)
            {
                damage = 2;
            }
            else if (0.715 < power && power < 0.897)
            {
                damage = 2;
            }
            else if (0.897 < power && power < 1)
            {
                damage = 0;
            }

            // HPの計算処理
            currentEnemyHp -= damage;

            if (currentEnemyHp <= 0)
            {
                currentEnemyHp = 0;
                is_enemy_dead = true;

                score += 50;
                scoreText.text = "" + Mathf.FloorToInt(score).ToString();
            }

            hpText.text = "HP " + Mathf.FloorToInt(currentEnemyHp).ToString() + "/" + Mathf.FloorToInt(currentEnemyMaxHp).ToString();
            gaugeStack = 0;
        }
        
        if (timerCount <= 0)
        {
            timerCount = 0;
            m_isFade = true;
        }
        
        if (m_isFade)
        {
            count += 60.0f * Time.deltaTime;

            if (count >= m_fadeTiming)
            {
                SceneCarrier.SetScore(score);
                SceneManager.GetInstance().ChangeScene("Result", nextBGM);
            }
        }
        else
        {
            count = 0;
        }
    }
}


