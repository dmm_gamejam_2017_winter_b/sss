﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundTransition : MonoBehaviour {

	struct Background {
		public Image[] images;
		public float scalar;
	}

	int m_nBackgrounds = 3;
	Background[] m_Backgrounds;

	float m_CurScalar;
	float m_ScalarInterval;

	// Use this for initialization
	void Start () {
		m_Backgrounds = new Background[m_nBackgrounds];
		m_ScalarInterval = (1.0f / (m_nBackgrounds - 1));
		m_CurScalar = 0;

		for (int i = 0; i < m_nBackgrounds; i++) {
			m_Backgrounds [i] = new Background ();
			m_Backgrounds [i].images = new Image[3];
			m_Backgrounds [i].images [0] = GameObject.Find ("sky" + (i + 1).ToString ()).GetComponent<Image>();
			//m_Backgrounds [i].images [1] = GameObject.Find ("sea" + (i + 1).ToString ()).GetComponent<Image>();
			m_Backgrounds [i].images [2] = GameObject.Find ("beach" + (i + 1).ToString ()).GetComponent<Image>();
			m_Backgrounds [i].scalar = i * m_ScalarInterval;

			SetBgAlpha (m_Backgrounds [i], i == 0 ? 1 : 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		int startIdx = Mathf.Clamp((int)(m_CurScalar / m_ScalarInterval), 0, m_nBackgrounds - 1);
		int endIdx = Mathf.Min(startIdx + 1, m_nBackgrounds - 1);
		float scalarDiff = (m_Backgrounds [endIdx].scalar - m_Backgrounds [startIdx].scalar);
		float transitionScalar;

		if (scalarDiff == 0) transitionScalar = 0;
		else transitionScalar = Mathf.Clamp01((m_CurScalar - m_Backgrounds [startIdx].scalar) / scalarDiff);

		for (int i = 0; i < m_nBackgrounds; i++) {
			if (i == startIdx)
				SetBgAlpha (m_Backgrounds [i], 1 - transitionScalar);
			else if (i == endIdx)
				SetBgAlpha (m_Backgrounds [i], transitionScalar);
			else
				SetBgAlpha (m_Backgrounds [i], 0);
		}
	}

	void SetBgAlpha(Background bg, float alpha)
	{
		for (int i = 0; i < 3; i++) {
			if (bg.images [i] == null)
				continue;

			Color color = bg.images [i].color;
			color.a = alpha;
			bg.images [i].color = color;
		}
	}

	public void SetTransitionScalar(float scalar) {
		m_CurScalar = scalar;
	}
}
