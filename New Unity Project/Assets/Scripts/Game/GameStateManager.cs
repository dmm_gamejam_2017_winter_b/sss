﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour
{
	enum State { Pregame, InGame, Finish }
	State m_CurrentState;
	float m_StateTimer;

	Image m_ReadyImg;
	RectTransform m_ReadyXform;
	Vector2 m_ReadyOrigSize;
	Image m_StartImg;
	RectTransform m_StartXform;
	Vector2 m_StartOrigSize;
	Image m_FinishImg;
	RectTransform m_FinishXform;
	Vector2 m_FinishOrigSize;

	BackgroundTransition m_BgTransition;

	GirlSpawner m_GirlSpawner;

	Text m_TimerText;

	float m_GameLength = 30.0f;

	public AudioClip nextBGM;

	// Use this for initialization
	void Start ()
	{
		SceneManager.GetInstance ().score = 0;

		m_CurrentState = State.Pregame;
		m_StateTimer = 0;
		GameObject readyObj = GameObject.Find ("ready");
		m_ReadyImg = readyObj.GetComponent<Image> ();
		m_ReadyXform = readyObj.GetComponent<RectTransform> ();
		m_ReadyOrigSize = m_ReadyXform.sizeDelta;

		GameObject startObj = GameObject.Find ("start");
		m_StartImg = startObj.GetComponent<Image> ();
		m_StartXform = startObj.GetComponent<RectTransform> ();
		m_StartOrigSize = m_StartXform.sizeDelta;

		GameObject finishObj = GameObject.Find ("finish");
		m_FinishImg = finishObj.GetComponent<Image> ();
		m_FinishXform = finishObj.GetComponent<RectTransform> ();
		m_FinishOrigSize = m_FinishXform.sizeDelta;

		m_ReadyImg.color = new Color (1.0f, 1.0f, 1.0f, 0.0f);
		m_StartImg.color = new Color (1.0f, 1.0f, 1.0f, 0.0f);
		m_FinishImg.color = new Color (1.0f, 1.0f, 1.0f, 0.0f);

		m_TimerText = GameObject.Find ("time2").GetComponent<Text> ();
		m_TimerText.text = ((int)m_GameLength).ToString ();

		m_BgTransition = GameObject.Find ("BackgroundTransition").GetComponent<BackgroundTransition> ();

		m_GirlSpawner = GameObject.Find ("GirlSpawner").GetComponent<GirlSpawner> ();
		m_GirlSpawner.SetSpawningEnabled (false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_StateTimer += Time.deltaTime;
		switch (m_CurrentState) 
		{
		case State.Pregame:
			UpdatePregameState ();
			break;
		case State.InGame:
			UpdateInGame ();
			break;
		case State.Finish:
			UpdateFinish ();
			break;
		}
	}

	void UpdatePregameState()
	{
		if (m_StateTimer < 1.5f)
			UpdateTextOverlay (0, m_ReadyImg, m_ReadyXform, m_ReadyOrigSize);
		else
			UpdateTextOverlay (1.5f, m_StartImg, m_StartXform, m_StartOrigSize);

		if (m_StateTimer >= 3.0f) {
			m_CurrentState = State.InGame;
			m_GirlSpawner.SetSpawningEnabled (true);
			m_StateTimer = 0;
		}
	}

	void UpdateInGame()
	{
		m_TimerText.text = ((int)((m_GameLength + 1) - m_StateTimer)).ToString();

		m_BgTransition.SetTransitionScalar (Mathf.Clamp01(m_StateTimer / m_GameLength));

		m_GirlSpawner.SetRemainingTime (m_GameLength - m_StateTimer);

		if (m_StateTimer >= m_GameLength) {
			m_CurrentState = State.Finish;
			m_GirlSpawner.SetSpawningEnabled (false);
			m_StateTimer = 0;
		}
	}

	void UpdateFinish()
	{
		UpdateTextOverlay (0, m_FinishImg, m_FinishXform, m_FinishOrigSize);

		if (m_StateTimer > 2.0f) {
			SceneManager.GetInstance().ChangeScene("result", nextBGM);
		}
	}

	void UpdateTextOverlay(float startTime, Image img, RectTransform xform, Vector2 origSize)
	{
		float fullAlphaTime = 0.25f;
		float normalScaleTime = 0.25f;
		float fadeAlphaStartTime = 1.0f;

		float effectTime = m_StateTimer - startTime;

		float alphaScalar;
		float sizeScalar;

		if (effectTime < fadeAlphaStartTime) {

			alphaScalar = Mathf.Clamp01 (effectTime / fullAlphaTime);
			sizeScalar = 2 - Mathf.Clamp01 (effectTime / normalScaleTime);
		} else {
			alphaScalar = 1 - Mathf.Clamp01 ((effectTime - fadeAlphaStartTime) / fullAlphaTime);
			sizeScalar = 1 + Mathf.Clamp01 ((effectTime - fadeAlphaStartTime) / normalScaleTime);
		}

		Color color = img.color;
		color.a = alphaScalar;
		img.color = color;

		xform.sizeDelta = origSize * sizeScalar;
	}
}
