﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShooterController : MonoBehaviour {

    Image _target;

    void Start()
    {
        _target = GameObject.Find("target").GetComponent<Image>();
    }

    // Update is called once per frame
    public void Update()
    {
        Aim(this.gameObject, _target.gameObject, 90);
    }

    Vector2 Aim(GameObject rotationObject, GameObject targetObject, float angleOffset)
    {
        Vector3 posDif = targetObject.transform.position - rotationObject.transform.position;
        float angle = Mathf.Atan2(posDif.y, posDif.x) * Mathf.Rad2Deg;
        Vector3 euler = new Vector3(0, 0, angle + angleOffset);

        rotationObject.transform.rotation = Quaternion.Euler(euler);

        return posDif.normalized;
    }
}
