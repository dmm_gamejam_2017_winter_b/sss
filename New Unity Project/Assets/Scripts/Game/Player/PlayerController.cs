﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerController : MonoBehaviour {

    Slider _gauge;
    GameObject _target;
    public GameObject bullet;
    GameObject _game;
    GameObject _shooter;

    // Use this for initialization
    void Start () {
        // get gauge
        _gauge = GameObject.Find("gauge").GetComponent<Slider>();
        _target = GameObject.Find("target");
        _game = GameObject.Find("game");
        _shooter = GameObject.Find("shooter");
    }

    public float _gaugeValue = 0;
	// Update is called once per frame
	void Update () {
	}

    public void PlusGauge (float value) {
        _gaugeValue += value;
        if (_gaugeValue > 1)
        {
            _gaugeValue = 1;
        }
        else if (_gaugeValue < 0)
        {
            _gaugeValue = 0;
        }
        _gauge.value = _gaugeValue;
    }

    public void Shoot()
    {
        if (_gaugeValue > 0.1f)
        {
            PlusGauge(-0.1f);
            var transform = this.transform;
            GameObject obj = (GameObject)Instantiate(bullet, _shooter.transform.position, Quaternion.identity);
            // 作成したオブジェクトを子として登録
            obj.transform.SetParent(transform);
            obj.transform.SetAsFirstSibling();
            obj.transform.DOMove(Input.mousePosition, 0.5f).OnComplete(() => { Destroy(obj); });
        }
    }
}
