﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class bulletEvent : MonoBehaviour {
    
    public GameObject _Girl;
    public GameObject effect;
    GameObject _player;

    // Use this for initialization
    void Start () {
        _player = GameObject.Find("player");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "girl")
        {
            _Girl = collision.gameObject;
            if (Random.Range(0, 2) == 1)
            {
                _Girl.GetComponent<Girl>().OnTopHit();
            }
            else
            {
                _Girl.GetComponent<Girl>().OnBottomHit();
            }

			_Girl.GetComponent<AudioSource>().Play ();
			SceneManager.GetInstance ().score += 100;

            var transform = _Girl.transform;
            GameObject obj = (GameObject)Instantiate(effect, gameObject.transform.position, Quaternion.identity);
            // 作成したオブジェクトを子として登録
            obj.transform.SetParent(transform);
            obj.transform.SetAsFirstSibling();
            if (_Girl.GetComponent<Girl>().GetClothingState() != Girl.ClothingState.None)
            {
                collision.gameObject.transform.DOJump(collision.gameObject.transform.position, 10, 1, 0.3f);
            }
            obj.transform.DOScale(Vector3.zero, 0.5f).OnComplete(() => { Destroy(obj); });
            Destroy(gameObject);
        }
    }
}
