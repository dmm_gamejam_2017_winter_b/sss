﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetEvent : MonoBehaviour {

    Image _target;
    GameObject _player;
    PlayerController playerController;
    
    void Start () {
        _target = GameObject.Find("target").GetComponent<Image>();
        _player = GameObject.Find("player");
        playerController = _player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    public void Update() {
        transform.position = Input.mousePosition;
	}

    public void OnMouseDrag()
    {
        float sensitivity = 0.01f; // いわゆるマウス感度
        //float mouse_move_x = Input.GetAxis("Mouse X") * sensitivity;
        float mouse_move_y = Mathf.Abs(Input.GetAxis("Mouse Y") * sensitivity);
        playerController.PlusGauge(mouse_move_y);
    }

    public void OnMouseUp()
    {
        playerController.Shoot();
    }
}
