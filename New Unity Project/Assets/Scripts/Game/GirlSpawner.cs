﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlSpawner : MonoBehaviour {

	public GameObject m_GirlPrefab;
	public Transform m_GirlParentXform;

	Girl[] m_Girls;
	bool m_bSpawnEnabled;
	float m_RemainingTime;
	float m_TimeToSpawn;
	float m_MaxSpawnHeight = 200;
	float m_RightScreenEdge = 1280;
	float m_LeftScreenEdge = 0;

	Vector2 m_SpawnRate1 = new Vector2(2.0f, 4.0f);
	Vector2 m_SpawnRate2 = new Vector2(0.75f, 1.5f);
	Vector2 m_SpawnRate3 = new Vector2(0.3f, 0.6f);

	// Use this for initialization
	void Start () {
		m_Girls = new Girl[30];
		m_bSpawnEnabled = false;
		m_TimeToSpawn = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!m_bSpawnEnabled)
			return;

		Vector2 spawnRate;
		if (m_RemainingTime <= 10.0f)
			spawnRate = m_SpawnRate3;
		else if (m_RemainingTime <= 20.0f)
			spawnRate = m_SpawnRate2;
		else
			spawnRate = m_SpawnRate1;

		m_TimeToSpawn -= Time.deltaTime;
		if (m_TimeToSpawn <= 0) {
			for (int i = 0; i < m_Girls.Length; i++) {
				if (m_Girls [i] == null) {
					Girl newGirl = GameObject.Instantiate (m_GirlPrefab).GetComponent<Girl>();

					Vector2 startPos = new Vector2();
					Vector2 moveVec;
					float width = newGirl.GetComponent<RectTransform> ().rect.width;
					m_LeftScreenEdge = -(width / 2);
					m_RightScreenEdge = 1280 + (width / 2);
					if (Random.value < 0.5f) {
						startPos.x = m_LeftScreenEdge;
						moveVec = Vector2.right;
					} else {
						startPos.x = m_RightScreenEdge;
						moveVec = Vector2.left;
					}

					startPos.y = m_MaxSpawnHeight * Random.value;

					newGirl.transform.SetParent (m_GirlParentXform);
					newGirl.transform.position = new Vector3(startPos.x, startPos.y, 0);
					newGirl.SetMoveVector(moveVec);
					m_Girls [i] = newGirl;
					break;
				}
			}

			m_TimeToSpawn = spawnRate.x + (Random.value * (spawnRate.y - spawnRate.x));
		}

		for (int i = 0; i < m_Girls.Length; i++) {
			for (int j = i; j < m_Girls.Length; j++) {
				if (m_Girls [i] == null || m_Girls [j] == null)
					continue; 

				if (m_Girls [i].transform.position.y > m_Girls [j].transform.position.y) {
					Girl temp = m_Girls [i];
					m_Girls [i] = m_Girls [j];
					m_Girls [j] = temp;
				}
			}
		}

		int idx = 0;
		for (int i = 0; i < m_Girls.Length; i++) {
			if (m_Girls [i] != null) {
				m_Girls [i].transform.SetSiblingIndex (idx);
			}
		}

		for (int i = 0; i < m_Girls.Length; i++) {
			if (m_Girls [i] == null)
				continue;
			if ((m_Girls [i].GetMoveVector ().x > 0 && m_Girls [i].transform.position.x > m_RightScreenEdge)
			    || m_Girls [i].GetMoveVector ().x < 0 && m_Girls [i].transform.position.x < m_LeftScreenEdge) {
				GameObject.Destroy (m_Girls [i].gameObject);
				m_Girls [i] = null;
			}
		}
	}

	public void SetSpawningEnabled(bool bEnabled) {
		m_bSpawnEnabled = bEnabled;
	}

	public void SetRemainingTime(float time) {
		m_RemainingTime = time;
	}
}
