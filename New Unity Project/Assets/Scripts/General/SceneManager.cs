﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

  [System.Serializable]
  public struct SCENE_DATA {
    public string name;
    public AudioClip BGM;
  }

  public SCENE_DATA[] m_scenes;

  public Color fadeColor = Color.black; // フェードカラー
  private float m_fadeAlpha = 0.0f;     // 内部用フェードα
  public bool isFading { get; private set; }    // フェード中かどうか
  private bool m_isChanging = false;

	int derp;
	public int score { get; set; }

  [SerializeField]
  [Range(0.0f, 1.0f)]
  private float m_bgmVolumeMax = 1.0f;

  public float bgmVolumeMax {
    get {
      return m_bgmVolumeMax;
    }
    set {
      m_bgmVolumeMax = value;
      if(m_currentSource) {
        m_currentSource.volume = m_bgmVolumeMax;
      }
    }
  }

  void OnValidate() {
    bgmVolumeMax = m_bgmVolumeMax;
  }

  private AudioSource m_currentSource;
  private AudioSource m_nextSource;

  public void SetCurrentBGM(AudioClip currentClip) {
    m_currentSource.clip = currentClip;
    if(!m_currentSource.isPlaying) {
      m_currentSource.Play();
    }
  }
  private static SceneManager instance;
  public static SceneManager GetInstance() {
    if(!instance) {
      var inst = new GameObject();
      inst.AddComponent<SceneManager>();
      instance = inst.GetComponent<SceneManager>();
    }
    return instance;
  }

  void Awake() {
    if(instance) {
      Destroy(this.gameObject);
      return;
    }
    instance = this;
    isFading = false;

    var crSource = new GameObject("audio1");
    crSource.transform.parent = transform;
    crSource.AddComponent<AudioSource>();
    m_currentSource = crSource.GetComponent<AudioSource>();
    m_currentSource.loop = true;
    m_currentSource.volume = bgmVolumeMax;
    m_currentSource.Stop();

    var nxSource = new GameObject("audio2");
    nxSource.transform.parent = transform;
    nxSource.AddComponent<AudioSource>();
    m_nextSource = nxSource.GetComponent<AudioSource>();
    m_nextSource.loop = true;
    m_nextSource.volume = 0.0f;
    m_nextSource.Stop();

    DontDestroyOnLoad(this);
  }

  void Update() {


    // デバッグシーン遷移
    if (!Input.GetKey(KeyCode.F3)) {
      return;
    }
    for (int i = 0; i < m_scenes.Length && i < 9; ++i) {
       if(Input.GetKeyDown(KeyCode.Alpha1+i)) {
        ChangeScene(m_scenes[i].name, m_scenes[i].BGM);
      }
    }
  }

  // BGMのクロスフェード
  public void CrossFadeBGM(AudioClip nextBGM, float fadingTime) {
    m_nextSource.clip = nextBGM;
    StartCoroutine(_CrossFadeBGM(fadingTime));
  }

  // シーン切り替え
  public void ChangeScene(string name, AudioClip nextBGM = null, float fadingTime = 1.0f) {
    if (isFading) {
      return;
    }
    Debug.Assert(null!=nextBGM, "次のBGMが指定されていません！");
    CrossFadeBGM(nextBGM, fadingTime*2.0f);
    StartCoroutine(TransScene(name, fadingTime));
  }

  // GUI表示
  public void OnGUI() {
    if(isFading) {
      fadeColor.a = m_fadeAlpha;
      GUI.color = fadeColor;
      GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
    }
  }

  // BGMクロスフェード用コルーチン
  IEnumerator _CrossFadeBGM(float fadingTime) {
    float time = 0.0f;
    m_currentSource.volume = bgmVolumeMax;
    m_nextSource.volume = 0.0f;
    m_nextSource.Play();

    while(fadingTime >= time) {
      time += Time.fixedDeltaTime;
      m_currentSource.volume = Mathf.Lerp(bgmVolumeMax, 0.0f, time / fadingTime);
      m_nextSource.volume = Mathf.Lerp(0.0f, bgmVolumeMax, time / fadingTime);
      yield return 0;
    }

    // シーン切り替え中なら待つ
    while(m_isChanging) {
      yield return 0;
    }

    m_currentSource.Stop();
    var source = m_currentSource;
    m_currentSource = m_nextSource;
    m_nextSource = source;
  }

  // フェード用コルーチン
  IEnumerator TransScene(string name, float fadingTime) {
    // だんだん暗く
    isFading = true;
    m_isChanging = true;
    float time = 0.0f;
    while(time <= fadingTime) {
      time += Time.fixedDeltaTime;
      m_fadeAlpha = Mathf.Lerp(0.0f, 1.0f, Mathf.Min(time / fadingTime, 1.0f));
      
      yield return 0;
    }

    // シーン切り替え
    Application.LoadLevel(name);
    m_isChanging = false;

    // だんだん明るく
    time = 0.0f;
    while(time <= fadingTime) {
      time += Time.fixedDeltaTime;
      m_fadeAlpha = Mathf.Lerp(1.0f, 0.0f, Mathf.Min(time / fadingTime, 1.0f));
      yield return 0;
    }
    isFading = false;
  }
}
