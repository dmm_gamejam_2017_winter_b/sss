﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundPlayer : MonoBehaviour {

  [System.Serializable]
  public struct SE_SET {
    public string seName;     // SE呼び出し名
    public AudioClip clip;    // オーディオクリップ

    public override string ToString() {
      return seName;
    }
    public override bool Equals(object obj) {
      return seName == obj.ToString();
    }
    public override int GetHashCode() {
      return base.GetHashCode();
    }
  }

  public SE_SET[] seFileList;

  private AudioSource m_source;

  public static SoundPlayer s_instance { get; private set; }

  [Range(0.0f, 1.0f)]
  public float volume = 1.0f;

  void Awake() {
    if(null==s_instance) {
      s_instance = this;
    }
    // オーディオソース(再生機)を作成
    var source = new GameObject("source");
    source.transform.parent = transform;
    source.AddComponent<AudioSource>();
    m_source = source.GetComponent<AudioSource>();
    m_source.loop = false;
    m_source.volume = volume;
  }

  void OnDestroy() {
    if(this== s_instance) {
      s_instance = null;
    }
  }
  
  public static void PlaySE(string seName) {
    if(null==s_instance) {
      return;
    }
    var index = System.Array.IndexOf(s_instance.seFileList, seName);
    if(0 > index) {
      return;
    }
    var clip = s_instance.seFileList[index].clip;
    if (null == clip) {
      return;
    }
    s_instance.m_source.PlayOneShot(clip, s_instance.volume);
  }
}
