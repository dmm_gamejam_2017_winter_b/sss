﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultScene : MonoBehaviour
{

    private float count = 0.0f;
    private bool m_isFade = false;
    public float m_fadeTiming = 120.0f;

    public AudioClip currentBGM;
    public AudioClip nextBGM;
    public Text scoreText; //Text用変数
    private int score = 0;

    // Use this for initialization
    void Start()
    {
        scoreText.text = "";
        score = SceneCarrier.GetScore();
        scoreText.text = Mathf.FloorToInt(score).ToString();

        //SceneManager.GetInstance().SetCurrentBGM(currentBGM);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("space"))
        {
            m_isFade = true;
        }

        if (m_isFade)
        {
            count += 60.0f * Time.deltaTime;

            if (count >= m_fadeTiming)
            {
                SceneManager.GetInstance().ChangeScene("Title", nextBGM);
            }
        }
        else
        {
            count = 0;
        }
    }
}


