﻿using UnityEngine;
using System.Collections;

public class SceneCarrier : MonoBehaviour {

    private static int m_score = 147;
    private static bool m_isDelete = false;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
        m_score = 0;
        m_isDelete = false;
	}

    void Update()
    {
        if (m_isDelete)
        {
            DestroyObject(this);
        }
    }

    public static void Delete()
    {
        m_isDelete = true;
    }

    public static void SetScore(int score)
    {
        m_score = score;
    }

    public static int GetScore()
    {
        return m_score;
    }

    public static void AddScore(int score)
    {
        m_score += score;
    }

    public static void ResetScore()
    {
        m_score = 0;
    }
}
