﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleScene : MonoBehaviour
{

    private float count = 0.0f;
    public bool m_isFade = false;
    public float m_fadeTiming = 120.0f;

    public AudioClip currentBGM;
    public AudioClip nextBGM;
    public AudioSource audioSource;
    // Use this for initialization
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.clip = currentBGM;
        audioSource.Play();
        //SceneManager.GetInstance().SetCurrentBGM(currentBGM);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("space"))
        {
            m_isFade = true;
        }

        if (m_isFade)
        {
            count += 60.0f * Time.deltaTime;

            if (count >= m_fadeTiming)
            {
                SceneManager.GetInstance().ChangeScene("seanscene", nextBGM);
            }
        }
        else
        {
            count = 0;
        }
    }
}

