﻿using UnityEngine;
using System.Collections;

public class sound : MonoBehaviour
{
    public AudioClip se;

    void Start()
    {
        GetComponent<AudioSource>().PlayOneShot(se);
    }
}